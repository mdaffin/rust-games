use bevy::{
    asset::{AssetLoader, LoadContext, LoadedAsset},
    prelude::*,
    render::pass::ClearColor,
    sprite::collide_aabb::{collide, Collision},
    reflect::TypeUuid,
    utils::BoxedFuture,
};
use bevy_prototype_lyon::prelude::*;

use anyhow::Result;
use wasmer::{imports, Instance, Module, NativeFunc, Store};
use wasmer_compiler_cranelift::Cranelift;
use wasmer_engine_jit::JIT;

/// An implementation of the classic game "Breakout"
fn main() {
    App::build()
        .add_plugins(DefaultPlugins)
        .add_resource(Scoreboard {
            player1: 0,
            player2: 0,
        })
        .add_resource(ClearColor(Color::rgb(0.9, 0.9, 0.9)))
        .add_asset::<WasmModule>()
        .init_asset_loader::<WasmModuleLoader>()
        .add_startup_system(setup.system())
        .add_system(ball_movement_system.system())
        .add_system(ball_collision_system.system())
        .add_system(paddle_movement_system.system())
        .add_system(scoreboard_system.system())
        .run();
}

struct Paddle(Player);

enum Player {
    Player1,
    Player2,
}

struct Ball {
    velocity: Vec3,
}

enum Collider {
    Solid,
    Scorable(Player),
}

struct Scoreboard {
    player1: usize,
    player2: usize,
}

struct WasmBundle {
    module: Handle<WasmModule>,
}

fn setup(
    commands: &mut Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut meshes: ResMut<Assets<Mesh>>,
    asset_server: Res<AssetServer>,
) {
    asset_server.watch_for_changes().unwrap();

    // Add walls
    let wall_material = materials.add(Color::rgb(0.8, 0.8, 0.8).into());
    let wall_thickness = 50.0;
    let bounds = Vec2::new(900.0, 600.0);

    // Add the game's entities to our world
    commands
        // cameras
        .spawn(Camera2dBundle::default())
        .spawn(CameraUiBundle::default())
        .spawn(SpriteBundle {
            material: materials.add(Color::rgb(0.5, 0.5, 1.0).into()),
            transform: Transform::from_translation(Vec3::new(-bounds.x / 2.0 + 50.0, 0.0, 0.0)),
            sprite: Sprite::new(Vec2::new(20.0, 100.0)),
            ..Default::default()
        })
        .with(Paddle(Player::Player1))
        .with(WasmBundle {
            module: asset_server.load("player_move1.wasm"),
        })
        .with(Collider::Solid)
        .spawn(SpriteBundle {
            material: materials.add(Color::rgb(0.5, 0.5, 1.0).into()),
            transform: Transform::from_translation(Vec3::new(bounds.x / 2.0 - 50.0, 0.0, 0.0)),
            sprite: Sprite::new(Vec2::new(20.0, 100.0)),
            ..Default::default()
        })
        .with(Paddle(Player::Player2))
        .with(WasmBundle {
            module: asset_server.load("player_move2.wasm"),
        })
        .with(Collider::Solid)
        .spawn(primitive(
            materials.add(Color::rgb(1.0, 0.5, 0.5).into()),
            &mut meshes,
            ShapeType::Circle(15.0),
            TessellationMode::Fill(&FillOptions::default()),
            Vec3::new(0.0, 0.0, 1.0).into(),
        ))
        .with(Ball {
            velocity: 400.0 * Vec3::new(0.5, -0.5, 0.0).normalize(),
        })
        .spawn(TextBundle {
            text: Text {
                font: asset_server.load("FiraSans-Bold.ttf"),
                value: "0 : 0".to_string(),
                style: TextStyle {
                    color: Color::rgb(0.5, 0.5, 1.0),
                    font_size: 40.0,
                    ..Default::default()
                },
            },
            style: Style {
                ..Default::default()
            },
            ..Default::default()
        });

    commands
        // left
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(-bounds.x / 2.0, 0.0, 0.0)),
            sprite: Sprite::new(Vec2::new(wall_thickness, bounds.y + wall_thickness)),
            ..Default::default()
        })
        .with(Collider::Scorable(Player::Player1))
        // right
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(bounds.x / 2.0, 0.0, 0.0)),
            sprite: Sprite::new(Vec2::new(wall_thickness, bounds.y + wall_thickness)),
            ..Default::default()
        })
        .with(Collider::Scorable(Player::Player2))
        // bottom
        .spawn(SpriteBundle {
            material: wall_material.clone(),
            transform: Transform::from_translation(Vec3::new(0.0, -bounds.y / 2.0, 0.0)),
            sprite: Sprite::new(Vec2::new(bounds.x + wall_thickness, wall_thickness)),
            ..Default::default()
        })
        .with(Collider::Solid)
        // top
        .spawn(SpriteBundle {
            material: wall_material,
            transform: Transform::from_translation(Vec3::new(0.0, bounds.y / 2.0, 0.0)),
            sprite: Sprite::new(Vec2::new(bounds.x + wall_thickness, wall_thickness)),
            ..Default::default()
        })
        .with(Collider::Solid);
}

fn ball_movement_system(time: Res<Time>, mut ball_query: Query<(&Ball, &mut Transform)>) {
    // clamp the timestep to stop the ball from escaping when the game starts
    let delta_seconds = f32::min(0.2, time.delta_seconds());

    for (ball, mut transform) in ball_query.iter_mut() {
        transform.translation += ball.velocity * delta_seconds;
    }
}

fn scoreboard_system(scoreboard: Res<Scoreboard>, mut query: Query<&mut Text>) {
    for mut text in query.iter_mut() {
        text.value = format!("{} : {}", scoreboard.player1, scoreboard.player2);
    }
}

fn ball_collision_system(
    mut scoreboard: ResMut<Scoreboard>,
    mut ball_query: Query<(&mut Ball, &mut Transform)>,
    collider_query: Query<(Entity, &Collider, &Transform, &Sprite)>,
) {
    for (mut ball, mut ball_transform) in ball_query.iter_mut() {
        // Sprites from bevy_prototype_lyon are always 1.0, 1.0 so cannot be used
        let ball_size = Vec2::new(30.0, 30.0);
        let velocity = &mut ball.velocity;

        // check collision with walls
        for (_collider_entity, collider, transform, sprite) in collider_query.iter() {
            let collision = collide(
                ball_transform.translation,
                ball_size,
                transform.translation,
                sprite.size,
            );

            match collider {
                Collider::Solid => {
                    if let Some(collision) = collision {
                        let reflect_x = match collision {
                            Collision::Left => velocity.x > 0.0,
                            Collision::Right => velocity.x < 0.0,
                            _ => false,
                        };

                        if reflect_x {
                            velocity.x = -velocity.x;
                        }

                        let reflect_y = match collision {
                            Collision::Top => velocity.y < 0.0,
                            Collision::Bottom => velocity.y > 0.0,
                            _ => false,
                        };

                        if reflect_y {
                            velocity.y = -velocity.y;
                        }
                    }
                }
                Collider::Scorable(player) => {
                    if let Some(_collision) = collision {
                        *ball_transform = Transform::from_translation(Vec3::new(0.0, 0.0, 0.0));
                        match player {
                            Player::Player1 => scoreboard.player2 += 1,
                            Player::Player2 => scoreboard.player1 += 1,
                        }
                    }
                }
            }
        }
    }
}

type PlayerMove = NativeFunc<(f32, f32), f32>;

fn paddle_movement_system(
    time: Res<Time>,
    wasm_asset: ResMut<Assets<WasmModule>>,
    ball_query: Query<(&Ball, &Transform)>,
    mut player_query: Query<(&Paddle, &mut Transform, &Sprite, &WasmBundle)>,
) {
    let (_, ball) = ball_query.iter().next().unwrap();
    let ball = &ball.translation;

    for (_, mut transform, sprite, wasm) in player_query.iter_mut() {
        if let Some(wasm) = wasm_asset.get(&wasm.module) {
            let import_object = imports! {};
            let instance = Instance::new(&wasm.0, &import_object).expect("instantiate module");

            let player_move: PlayerMove = instance
                .exports
                .get_native_function("player_move")
                .expect("player_move function in Wasm module");

            let player = &mut transform.translation;
            let direction = player_move.call(player.y, ball.y).unwrap();
            player.y += time.delta_seconds() * direction.signum() * 200.0;

            // bound the paddle within the walls
            let bounds = 600.0 / 2.0 - sprite.size.y / 2.0;
            player.y = player.y.min(bounds).max(-bounds);
        }
    }
}

#[derive(Clone)]
pub struct WasmModuleLoader {
    store: Store,
}

#[derive(Debug, Clone, TypeUuid)]
#[uuid = "d4bd6b78-108f-4644-8265-d8b894984127"]
pub struct WasmModule(Module);

impl Default for WasmModuleLoader {
    fn default() -> Self {
        WasmModuleLoader {
            store: Store::new(&JIT::new(Cranelift::default()).engine()),
        }
    }
}

impl AssetLoader for WasmModuleLoader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<()>> {
        Box::pin(async move {
            let module = WasmModule(Module::new(&self.store, bytes)?);
            load_context.set_default_asset(LoadedAsset::new(module));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["wasm"]
    }
}
